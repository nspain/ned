#!/usr/bin/env bash4
### Script for running tests of ned -- Nick's Ed.

# Environment
testdir="tests"
executable="./ned.o"

# Test variables
total_tests=0
passed_tests=0
failed_tests=0

################################################################################
### DO THIS WITH ANOTHER C FILE
## Testing low-level functionality
## Unit tests  for the function in  ned's source code and  also testing
## the DFA produced by regexps.

## Unit tests
# Tests each function specifically.
# How to do this for void functions?
#     - Do I need to change the structure?
#         - More functions, each with one task?
#         - Minimize void functions, have a few /impure/ functions
#             that act as tees


## Regexp
# Tests that the regexps are consistent with UNIX regexps.

################################################################################
## Testing top-evel functionality
## Test the ned's commands and modes.

# Run tests for list of files
function run {
    for f in "$@"
    do
    	printf "Testing: $f"
    	"$executable" < "$testdir/$f" > /dev/null
    	if [[ "$?" != 0 ]]
    	then
    	    printf "... FAILED\n"
    	    ((failed_tests++))
    	else
    	    printf "... PASSED\n"
    	    ((passed_tests++))
    	fi
    	((total_tests++))
    done
}

# Compare the command output to the expected output of a command
function cmp_output {
    for f in "$@"
    do
	output="$testdir/output-$f"
	expected="$testdir/expected-$f"
	printf "Testing: $f"
	"$executable" < "$testdir/$f" > "$output"
	diff "$expected" "$output" > /dev/null
	if [[ "$?" != 0 ]]
	then
	    printf "... FAILED\n"
	    ((failed_tests++))
	else
	    printf "... PASSED\n"
	    ((passed_tests++))
	fi
	((total_tests++))
    done
}

## Insert
# Test that the insert entry into editor mode is working correctly.
run "insert-test1.txt"

## Append
# Test that the append entry into editor mode is working correctly.
run "append-test1.txt"

## Change
# Test that the change entry into editor mode is working correctly.
run "change-test1.txt"

## Help
# Test that the help command 'h' print the correct messages.
cmp_output "help-test1.txt"


## Search
# Test that text search is working.

## Substitution
# Test that text substitution is working.

## Write
# Test that write commands writes to current file.

## Command mode
# Test that the command '.' makes ned go from editor mode to command mode.

################################################################################
# Report test output
echo "";
echo "---------------------------------------------------------------------"
echo "Results:"

echo   "+---------+------+"
printf "| Total:  | %4d |\n" $total_tests
echo   "|---------+------|"
printf "| Passed: | %4d |\n" $passed_tests
echo   "|---------+------|"
printf "| Failed: | %4d |\n"  $failed_tests
echo   "+---------+------+"
