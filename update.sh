#!usr/bin/env bash4
# Update all files in main directory using awk-update.awk and awk-bump.awk.

date=$(date "+%Y-%m-%d")
if [[ $1 == "big" ]]
then
        echo "Big update"
        cp "$2" "$2.bak"
        awk -v ver=0 -v date="$date" -f awk-bump.awk < "$2" > tmp
elif [[ $1 == "med" ]]
then
        echo "Medium update"
        cp "$2" "$2.bak"
        awk -v ver=1 -v date="$date" -f awk-bump.awk < "$2" > tmp
else
        echo "Small update"
        cp "$1" "$1.bak"
        awk -v ver=2 -v date="$date" -f awk-bump.awk < "$1" > tmp
fi

mv tmp "$2"

