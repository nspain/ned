/* ned -- A modern implementation of the UNIX line based editor Ed.
 *
 * AUTHOR: Nick Spain <nicholas.spain96@gmail.com>
 * CREATED: 2016-12-05
 * UPDATED: 2016-12-05
 * VERSION 0.0.0
 */

#define MAXLINE    (1000) /* Maximum line length */
#define TOCMD      ('.')  /* Enter command mode */
#define INSERT     ('i')  /* Enter edit mode via insert */
#define APPEND     ('a')  /* Enter edit mode via append */
#define CHANGE     ('c')  /* Enter edit mode visa change */
#define SUB        ('s')  /* Prefix for substitution command */
#define SEARCH     ('/')  /* Prefix for search command */
#define QUIT       ('q')  /* Quit ned */

#define CMD       (1)     /* Buffer mode value when in command mode */
#define ED        (0)    /* Buffer mode value when in editor mode */
#define NEDSTREAM (stdin) /* Stream that ned reads from */


#include <stdio.h>
#include <stdlib.h>

/* Type abstractions */

typedef struct {
	ssize_t len;
	char *line;
} line_t;

/*
 * buffer_t has been rearranged to stop automatic padding (types in ascending
 * order).
 */
typedef struct {
	line_t *lines;		/* Array of lines in buffer */
        int pos;                /* Current position in buffer */
	int linum;              /* Number of lines in buffer */
	size_t size;    	/* Buffer size in bytes, for mallocing */
        int mode;               /* Buffer mode (CMD or ED) */
        int pad;                /* Prevent auto padding of struct */
} buffer_t;

void exitnull (void*, char*);
int isquit (int);
void initbuffer (buffer_t *buffer, FILE *file);
int parseline(line_t*, buffer_t*);
void writetobuf(line_t*, buffer_t*);
char getcmd (line_t*);
