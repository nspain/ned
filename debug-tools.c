/* debugging-tools.c --- Macros/functions for debugging. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

void PRINTINT (char *var, int n);
void PRINTSTR (char *var, char *str);
static int intlen (int n);
static void itoa (char* dest, int src);

#ifdef DEBUG
#define ASSERT(msg, exp) do {				        \
        if (!(exp)) {                                           \
            fprintf(stderr, "Error:%s:%s:%d: %s",		\
		    __FILE__, __FUNCTION__, __LINE__, msg);	\
            exit(EXIT_FAILURE);                                 \
        }                                                       \
    } while(0)

/* Convert N to a string and print it with its variable name VAR. */
void PRINTINT (char *var, int n) {
    char *str;
    str = malloc(sizeof(*str) * (size_t)(intlen(n) + 1));
    assert(str);
    itoa(str, n);
    printf("debug:printint: %s = %s\n", var, str);
    free(str);
    str = NULL;
    return;
}

/* Print string STR with its variable name VAR. */
void PRINTSTR (char *var, char *str) {
    printf("debug:printstr: %s = %s\n", var, str);
    return;
}
#else
#define ASSERT(msg,exp)
static
#endif


/* Return the number of digits in N. */
static int intlen (int n) {
    int digits;
    assert(n >= 0);
    if (n == 0) {
	digits = 1;
    } else {
	digits = (int) (log((double) n)/log(10.0) + 1.0);
    }
    return digits;
}

/* Convert SRC to a string and put it into DEST.
 * Function assumes DEST has been allocated with at least digits+1
 * bytes, where digits is the number of digits in SRC. */
static void itoa (char *dest, int src) {
    int len;
    len = intlen(src);
    dest[len--] = '\0';
    if (len == 0) {
	dest[len] = '0';
    } else {
	/* The LEN check is just to prevent memory overflow. */
	while (src && len >= 0) {
	    dest[len] = src % 10 + (int)'0';
	    src /= 10;
	    len -= 1;
	}
	/* If SRC not 0 then we exited because LEN drop below zero, so
	 * DEST is wrong */
	assert(src == 0);
    }
    return;
}
