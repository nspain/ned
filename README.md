# NED

Welcome to Nick's Ed (ned), my implementation of the UNIX line
orientated editor, ed. This is my first deep foray into C programming
so I though that I'd start were it all began, much of the UNIX
operating system was written using ED.

# Try it out
If you're running a *nix based OS (osx, gnu+linux, etc) try typing
```ed``` into your command line. If you try and type anthing, you'll
probably get ```?``` most of the time. I'll go though a few of the
commands to get you going but first I need to get something out of the
way. In ```ed``` there are two *modes*. ```Insert``` and ```Command```.
```Insert``` mode is for inserting text and ```Command``` mode is for running
commands.

## Command mode
* ```a``` - append, start inserting text in the line below your current line.
* ```i``` - insert, start inserting text in the line above your current line.
* ```c``` - change, start inserting text over the current line.
* ```d``` - delete, delete the current line.
* ```p``` - print, print the current line.
* ```h``` - Print what the hell ```?``` was refering to!

Commands can be prefix with line numbers to call the command on that
line. The editor will then move to that line. For example, ```5,10p```
will print lines 5 to 10 and ```,p``` will print all the lines in the
file.

## Insert mode

```.``` - Get out of insert mode
This command needs to be on a line of its own.

# My version

Now that you have a taste of what ```ed``` is like, I'll explain what
differences mine will entail. The first one is that by defaul thare
will be a prompt indicating command mode. You probably noticed that,
by default, when you enter ```ed``` there is not visible way to
distinguish whether you're in command or insert mode. ```ned``` will
have an asterix ('*') to tell you you're in command mode. The second
is that the executable will by under 100kb. ```ed``` is a very minimal
editor because it was developed in a time when computer memory was
very expensive. Sometimes it's nice to go back to those days and
minimise feature creep.
