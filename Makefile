INFILE = ned.c
OUTFILE = ned.o
TESTRUNNER = run-tests.sh
FLAGS = -Weverything

build: $(INFILE)
	$(CC) $(FLAGS) -o $(OUTFILE) $(INFILE)

debug: $(INFILE)
	$(CC) $(FLAGS) -W -o0 -g -o $(OUTFILE) $(INFILE)

clean:
	rm *.o && rm -rf $(OUTFILE).dSYM

test:
	./$(TESTRUNNER)

rebuild: clean build
