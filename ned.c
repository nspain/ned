/* ned -- A modern implementation of the UNIX line based editor Ed.
 *
 * AUTHOR: Nick Spain <nicholas.spain96@gmail.com>
 * CREATED: 2016-12-05
 * UPDATED: 2016-12-05
 * VERSION: 0.0.0
 */

#include "ned.h"

int main (int argc, char *argv[]) {
        int quit;
        size_t linecap = 0;
        FILE *file;
        line_t line;
        buffer_t buffer;
        if (argc > 1) {
                file = fopen(argv[1], "a+"); // a+ creates or appends to file
                printf("Opening %s.\n", argv[1]);
                exitnull(file, "Unable to open file");
        } else {
                file = NULL;
                printf("No file specified.\n");
                printf("Use `f <file-name>` before writing to file.\n");
        }

        initbuffer(&buffer, file);
        line.line = NULL;

        quit = 0;
        while (!isquit(quit)) {
                line.len = getline(&(line.line), &linecap, NEDSTREAM);
                if (line.len == -1 || line.line == NULL) {
                        exitnull(NULL, "`getline` error"); // Little hackish
                }
                quit = parseline(&line, &buffer);
        }
        printf("Goodbye.\n");
        if (file) fclose(file);
        return 0;
}

/* If EXPR is NULL, print MSG and exit program. */
void exitnull (void *expr, char *msg) {
        if (expr == NULL) {
                printf("%s.\n", msg);
                exit(EXIT_FAILURE);
        }
        return;
}

/* If QUIT is truthy, return 1. Otherwise return 0. */
int isquit (int q) {
        return !q;
}

/* Initialise BUFFER, if FILE is not null, read it into BUFFER. */
void initbuffer (buffer_t *buf, FILE *file) {
        (void) buf;
        (void) file;
        return;
}

/* Parse LINE, if necessary run commands on BUFFER. If QUIT command is called
 * return 1 for quitting, otherwise return false. */
int parseline(line_t *line, buffer_t *buf) {
        char cmd;
        cmd = getcmd(line);
        if (buf->mode == CMD) {
                switch (cmd) {
                        case INSERT:
                                break;
                        case APPEND:
                                break;
                        case CHANGE:
                                break;
                        case SUB:
                                break;
                        case SEARCH:
                                break;
                        case QUIT:
                                return 1;
                        default:
                                printf("?\n");
                }
        } else if (buf->mode == ED && cmd == TOCMD) {
                buf->mode = CMD;
        } else {
                writetobuf(line, buf);
        }
        return 0;
}

/* Write LINE to BUFFER and increment buffer pos. If necessary, jostles lines
 * around to get into right position.
 */
void writetobuf (line_t *line, buffer_t *buf) {
        (void) line;
        (void) buf;
        return;
}

/* Get command char from LINE. */
char getcmd (line_t *line) {
        (void) line;
        return 0;
}

