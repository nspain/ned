#!/usr/bin/awk
# Bump VERSION field for a given file.
BEGIN {
        FS = ":";
        OFS = ": ";
}
{
        if (match($0, /VERSION/)) {
                split($2, a, ".");
                a[ver]+=1;
                if (ver == 0) {
                        a[1] = a[2] = 0;
                } else if (ver == 1) {
                        a[2] = 0;
                }
                print $1, sprintf("%d.%d.%d", a[0], a[1], a[2])
        } else if (match($0, /UPDATED/)) {
                print $1, date
        } else {
                print $0
        }
}
